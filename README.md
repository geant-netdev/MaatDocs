# Welcome to the Maat documentation

These pages contain the information regarding Maat and the user documentation including how to install and configure and how to use Maat.

The content of this website is provide under CC-BY-4.0 unless stated otherwise.

Developed by WP6 T2 Platform, part of the GÉANT GN5-1 project.