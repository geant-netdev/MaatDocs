---
title: "Concepts"
hide:
    - toc
    - navigation
---


# Maat as a Single Source of Truth


<div class="grid cards" markdown>

-   :material-graph-outline:{ .lg .middle } __Redefine how you view and think of the network__

-   :fontawesome-brands-connectdevelop:{ .lg .middle } __Maat helps you accurately design and describe network resources and services.__

-   :material-star-check:{ .lg .middle } __Build trust in your Single Source of Truth.__

</div>

 

## Why do you need a Single Source of Truth?

Your network single source of truth should provide you with a complete, detailed view of all of your network assets including hardware, software, configuration details, connections, relationships and other relevant information. 

Not paying enough attention to your inventory information can lead to many problems such as:

- Being unable to correctly plan network changes
- Running into difficulties and issues when trying to implement maintenance operations
- Having trouble understanding the impact of faults on resources and/or services
- Using a partial view of the network due lack of information when using a vendor specific solution

Most importantly, maintaining *a Single Source of Truth is essential for implementing automation and orchestration*!

## Maat can help you!

<div class="grid cards" markdown>

-   :material-source-branch-check:{ .lg .middle } __Vendor Agnostic__

    ---

    Maat offers a unified and federated view of network resources and services in a single source of truth that supports multi-vendor and multi-technology networks.


-   :material-api:{ .lg .middle } __Open API__

    ---

    It enables automation of service and resource lifetime management via a flexible open API and notification manager.


-   :material-fast-forward:{ .lg .middle } __Made to measure__

    ---

    Only by using an accurate and flexible single source of truth can you fast-track service delivery and solve problems proactively.


-   :material-scale-balance:{ .lg .middle } __Optimised__

    ---

    The consolidated view of the network simplifies decision making, minimizes costs and improves resource management.

</div>

   

### Why not let Maat become your single source of truth and drive your network automation and orchestration?

## Benefits

=== "Complete end-to-end model of the network and services"

    - Accurate representation from customer to any network device
    - Detailed view across technologies and systems

=== "Unified and federated"

    - Layered view of physical, logical, and/or virtual network and services
    - Design and document all layers

=== "Dynamic"

    - Highly scalable single source of truth
    - Adjustable data model and GUI
    - Data-driven cataloguing 

=== "Standards based"

    - Implements the TM Forum Open API for resource and service management
    - Open-source forever
    - Freely available under a permissible license, enabling you to further customize
    - Easily develop plug-ins and extensions 
    
=== "Innovations ready"

    - The future-proof design and architecture supports SDN and NFV based resources and services, supporting a mix of virtualized and fixed networks

<br><br><br>
![decorative hieroglyphs](../assets/images/Maat%20graphics/horizontal%20hieroglyphs/grey2.png)
