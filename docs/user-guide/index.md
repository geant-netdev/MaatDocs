---
title: "Maat User Guide"
hide:
    - toc
---

# Maat User Guide

Maat is a single source of truth application that:

- stores information about resources and services
- exposes two Open APIs:
    - TMF 638 Service Inventory 
    - TMF 639 Resource Inventory

It is powered by :simple-springboot: Spring Boot 3.0 and NoSQL databases :simple-mongodb: MongoDB.

API access can be encrypted using SSL and authenticated using OAuth 2.0 powered by :simple-keycloak: Keycloak.

## How to Install and Configure Maat

=== "Install"

    You can install and use Maat in several ways:

    * install from source
    * use a pre-prepared Docker instance

    Together with Maat you can choose to install the Event Listener and/or Keycloak with SSL.

    [:octicons-arrow-right-24: Go to Install](./01%20Installation/install.md)


=== "Configure"

    Maat has a number of parameters that enable you to configure and customise it to your liking.

    [:octicons-arrow-right-24: Start from the basic configuration](./02%20Configuration/basic_configure.md)

    [:octicons-arrow-right-24: Configure SSL](./02%20Configuration/ssl_configure.md)

    [:octicons-arrow-right-24: Configure authentication](./02%20Configuration/api_configure.md)


<div class="grid cards" markdown>

-   :material-api:{ .lg .middle } __Maat API__

    ---

    There are two APIs available: Service Management API and Resource Management API.

    No matter what you use the data will be validated against a schema you can freely customise.

     [:octicons-arrow-right-24: How to use the REST APIs](./03%20Maat%20API/restAPI.md)

     [:octicons-arrow-right-24: Data Validation](./03%20Maat%20API/validation.md)

-   :material-hexagon-multiple-outline:{ .lg .middle } __Example API requests__

    ---

    Take a look at some of the examples on how to make API requests and manage the data stored in Maat.

     [:octicons-arrow-right-24: View example API calls](./04%20Examples/example.md)

-   :simple-postman:{ .lg .middle } __POSTMAN__

    ---

    A Postman collection that can be used to to test the REST APIs is available in the [src/main/resources](https://bitbucket.software.geant.org/projects/OSSBSS/repos/inventory3/browse/src/main/resources) folder

    [:octicons-arrow-right-24: Go to the Postman collection](https://bitbucket.software.geant.org/projects/OSSBSS/repos/inventory3/browse/src/main/resources/Inventory3_Test.postman_collection.json)

-   :simple-swagger:{ .lg .middle } __SWAGGER UI__

    ---

    Maat also offers a Swagger UI. Use the [examples](./04%20Examples/example.md) for sample services and resources when you use the POST method.
    
     [:octicons-arrow-right-24: Go to Swagger UI](http://localhost:8080/swagger-ui/index.html)

</div>