---
title: "Data Management"
---
# MongoDB Data Management

## MongoDB backup data

To create a copy of the MongoDB database from the container or restore the data to database follow the steps below:

a. create backup:

  1. use "mongodump" tool in MongoDB container

    ```docker exec -i <container_id> /usr/bin/mongodump --username <username> --password <password> --out /dump```

  2. copy created data from container to host machine

    ```docker cp <container_id>:/dump /home/service/MongodbBackup/Maat```

b. restore data:

  1. copy data from host machine to MongoDB container

    ```docker cp ./dump <container_id>:/dump```

  2. use "mongorestore" tool in MongoDB container

    ```docker exec -i <container_id> /usr/bin/mongorestore --username <password> --password <password> /dump```

## MongoDB delete data

To delete data from MongoDB for resources_db, services_db and listeners_db follow the steps below:

- for resources: 

    ```docker exec -it <container_id> /usr/bin/mongosh --username <username> --password <password> --authenticationDatabase admin --eval "use resources_db;" --eval "db.dropDatabase()"```

- for services: 

    ```docker exec -it <container_id> /usr/bin/mongosh --username <username> --password <password> --authenticationDatabase admin --eval "use services_db;" --eval "db.dropDatabase()"```

- for listeners: 

    ```docker exec -it <container_id> /usr/bin/mongosh --username <username> --password <password> --authenticationDatabase admin --eval "use listeners_db;" --eval "db.dropDatabase()"```

- for all databases: 

    ```docker exec -it <container_id> /usr/bin/mongosh --username <username> --password <password> --authenticationDatabase admin --eval "use resources_db;" --eval "db.dropDatabase()" --eval "use services_db;" --eval "db.dropDatabase()" --eval "use listeners_db;" --eval "db.dropDatabase()"```