---
title: "REST API"
hide:
    - toc
---

Maat REST APIs are compliant with the TMForum:

- [TMF 639 Resource Inventory](https://github.com/tmforum-apis/TMF639_ResourceInventory)
- [TMF 638 Service Inventory](https://github.com/tmforum-apis/TMF638_ServiceInventory)


|                                  Link                                  | Method | Input |       Description        |
|:----------------------------------------------------------------------:|:------:|:-----:|:------------------------:|
|   http://127.0.0.1:8080/resourceInventoryManagement/v4.0.0/resource    |  GET   |   -   |    Get all resources     |
| http://127.0.0.1:8080/resourceInventoryManagement/v4.0.0/resource/[ID] |  GET   |  ID   |    Get resource by ID    |
|   http://127.0.0.1:8080/resourceInventoryManagement/v4.0.0/resource    |  POST  | JSON  |       Add resource       |
| http://127.0.0.1:8080/resourceInventoryManagement/v4.0.0/resource/[ID] | DELETE |  ID   |  Delete resource by ID   |
| http://127.0.0.1:8080/resourceInventoryManagement/v4.0.0/resource/[ID] | PATCH  |  ID   | Update existing resource |
|    http://127.0.0.1:8080/serviceInventoryManagement/v4.0.0/service     |  GET   |   -   |     Get all services     |
|  http://127.0.0.1:8080/serviceInventoryManagement/v4.0.0/service/[ID]  |  GET   |  ID   |    Get service by ID     |
|    http://127.0.0.1:8080/serviceInventoryManagement/v4.0.0/service     |  POST  | JSON  |       Add service        |
|  http://127.0.0.1:8080/serviceInventoryManagement/v4.0.0/service/[ID]  | DELETE |  ID   |   Delete service by ID   |
|  http://127.0.0.1:8080/serviceInventoryManagement/v4.0.0/service/[ID]  | PATCH  |  ID   | Update existing service  |

## Filtering

For GET method you can use additional params to filter resources or services more accurately:

- limit *--to limit the number of displayed objects*
- offset *--to make offset on received objects*
- fields *--to display only selected fields (for example: fields=name,description)*

There is also the possibility to search elements by key-value option. 

- for example, using the parameter "name=resource1" will filter all resources or services attributes "name" with value "resource1".

## Automatic reference completion

Maat has an automatic reference completion, the so-called backward reference (relationship) generation. For more information go to [Backward relationships between objects](./backward_relationship.md)