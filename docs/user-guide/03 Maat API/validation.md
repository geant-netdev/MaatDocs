---
title: "Schema Validation"
hide:
    - toc
---
# Request validation

Every resource or service added to the Maat via REST API is validated.

Validation is performed using a schema that defines the appropriate attributes and relationships according to the TMF standards (TMF 638 Service Inventory and TMF 639 Resource Inventory APIs).

Schema location for validation in the POST request is located in the @schemaLocation attribute.

This attribute can contain public <b>[GitHub](https://github.com/GEANT-NETDEV/Inv3-schema)</b> address:

- for resource validation: https://raw.githubusercontent.com/GEANT-NETDEV/Inv3-schema/main/TMF639-ResourceInventory-v4-ext_20240121-1.json
- for service validation: https://raw.githubusercontent.com/GEANT-NETDEV/Inv3-schema/main/TMF638-ServiceInventory-v4.json

or a file path

- for Linux: 
  - file:///home/Maat/schema/TMF639-ResourceInventory-v4.json
  - file:///home/Maat/schema/TMF638-ServiceInventory-v4.json

- for Windows:
  - file:///C:/Users/schema/TMF639-ResourceInventory-v4.json
  - file:///C:/Users/schema/TMF638-ServiceInventory-v4.json

## Non-TMF schema validation

The schema file does not have to follow the TMF standard. 

It can be simplified to address user requirements regarding data models. 

An example of simple schema files for resources and services can be found here:

- for resource validation: https://raw.githubusercontent.com/GEANT-NETDEV/Inv3-schema/main/ResourceInventory-example-1.json
- for service validation: https://raw.githubusercontent.com/GEANT-NETDEV/Inv3-schema/main/ServiceInventory-example-1.json

A Postman collection for testing requests with the above schema files is available at [Example_with_simple_schema.postman_collection.json](https://bitbucket.software.geant.org/projects/OSSBSS/repos/Maat/browse/src/main/resources/Example_with_simple_schema.postman_collection.json)