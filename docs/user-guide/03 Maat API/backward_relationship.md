---
title: "Backward relationships between objects"
hide:
    - toc
---

# Backward Relationships

Maat has an automatic reference completion, the so-called backward reference (relationship) generation. This is based on the fact that:

1. when a resource/service A that has a reference to resource/service B (relationship A->B) is created or updated, 
2. a **backward reference** to resource/service A (relationship B->A) is automatically created in resource/service B as well.

To activate the creation of backward references, the **prefix "bref" in the relationshipType attribute** of the resourceRelationship element must be used. The second condition is also to add (after the prefix) the name of the resource/service category in the relationshipType to which the reference is created.  

## Example

Create (REST API POST method) a new resource with the relationship to the existing resource id="Res-123" and force generation of the backward reference to the resource to be created:

```
{
    "name": "test",
    "category": "testCategoryB",
    "@type": "LogicalResource",
    "@schemaLocation": "https://raw.githubusercontent.com/GEANT-NETDEV/Inv3-schema/main/TMF639-ResourceInventory-v4-pionier.json",
    "resourceRelationship": [
      {
        "relationshipType": "bref:testCategoryA",
        "resource": {
          "id": "Res-123",
          "href": "http://localhost:8080/resourceInventoryManagement/v4.0.0/resource/Res-123"
        }
      }
    ]
}
```

When such a POST request is received and accepted (validation is correct) by Maat, the following backward reference to the newly created resource is added to the above resource id="Res-123":

```
"resourceRelationship": [
            {
                "relationshipType": "ref:testCategoryB",
                "resource": {
                    "id": "Res-new-456",
                    "href": "http://localhost:8080/resourceInventoryManagement/v4.0.0/resource/Res-new-456"
                }
            }
        ],
```

Relationships can occur in the following options: 

- resource :octicons-arrow-both-16: resource
- service :octicons-arrow-both-16: service 
- resource :octicons-arrow-both-16: service
- service :octicons-arrow-both-16: resource 

A reference to the resource is added using resourceRelationship, while serviceRelationship is used for a reference to the service. 

In addition, the name of the referenced resource/service can be automatically added. This can be helpful in some situations to limit the number of calls to Maat to retrieve the name of the resource/service referenced. For this purpose, the attribute name with the value "set-name"  must be placed in the relationship.

## Example

Create (REST API POST method) a new resource with a backward reference to the service id="Service-123" and with the name attribute having the value "set-name".

```
{
    "name": "test4name",
    "category": "testCategory",
    "@type": "LogicalResource",
    "@schemaLocation": "https://raw.githubusercontent.com/GEANT-NETDEV/Inv3-schema/main/TMF639-ResourceInventory-v4-pionier.json",
    "serviceRelationship": [
      {
        "relationshipType": "bref:testServiceCategory",
        "service": {
          "id": "Service-123",
          "href": "http://localhost:8080/serviceInventoryManagement/v4.0.0/service/Service-123",
          "name":"set-name"
        }
      }
    ]
}
```

When such a POST request is received and accepted (validation is correct) by Maat, the following backward reference to the newly created resource, with its name, is added to the service id="Service-123" .

```
"resourceRelationship": [
            {
                "relationshipType": "ref:testCategory",
                "resource": {
                    "id": "Res-new-456",
                    "href": "http://localhost:8080/resourceInventoryManagement/v4.0.0/resource/Res-new-456",
                    "name": "test4name"
                }
            }
        ]
```

The relationship in the newly created resource in such a case looks as follows:

```
 "serviceRelationship": [
        {
            "relationshipType": "bref:testServiceCategory",
            "service": {
                "id": "Service-123",
                "href": "http://localhost:8080/serviceInventoryManagement/v4.0.0/service/Service-123",
                "name": "testServiceName"
            }
        }
    ]
```
!!! warning

    The above functionalities cause that updating the "name" and "category" attributes is not allowed.
