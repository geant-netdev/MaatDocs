# How to Install Maat

## Installation from sources

### Requirements

Before you start make sure that you already have installed the following:

- :simple-apachemaven: Maven 3.8.1 (or higher versions)
- :fontawesome-brands-java: Java 17 (or higher versions)
- :simple-mongodb: NoSQL Database - MongoDB

### Before you build

Install MongoDB with admin account.

You can create admin account using mongosh and the following script:

```
use admin  
db.createUser(  
  {  
    user: "admin",  
    pwd: "abc123",  
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]  
  }  
)
```

### Build

```mvn clean install```

In case you want to skip running the tests during the installation then you can use the following command:

```mvn clean install -DskipTests```

In both cases, the *jar* application will be created in the *target* folder.

### Run

Go to **target/** folder and run jar file with created name for Maat.

``` java -jar Maat-0.9.1.jar ```

## Use Docker to run Maat containers

### Simply Maat

An alternative to the installation procedure is to simply use docker containers.


Go to the **docker/** folder and run:

```docker-compose up -d```


### Maat with EventListener

[EventListener](https://bitbucket.software.geant.org/projects/OSSBSS/repos/Maat-eventlistener) is a supporting application for storing notifications from Maat. 

- Notifications inform about any events (add/update/delete resources/services) in Maat.

- The EventListener automatically registers to Maat upon starting (address and port of Maat are provided in the properties Maat-host and Maat-port). 


Go to **docker/** folder and run:

```docker-compose -f docker-compose-2.yml up```


### Maat (and EventListener) with Keycloak and SSL

Go to **docker/** folder and run:

```docker-compose -f docker-compose-3.yml up```

After starting all services in containers, run container with setup for Keycloak:

```docker-compose --profile keycloak_setup -f docker-compose-3.yml up -d```

!!! warning 
        When Maat works with Keycloak and SSL you must manually register EventListener using the steps provided below:

1. Get access token

```curl -X POST --data "client_id=inv3&username=test&password=test&grant_type=password&client_secret=d0b8122f-8dfb-46b7-b68a-f5cc4e25d123" -H "Host: keycloakhost:8090" http://localhost:8090/realms/MaatRealm/protocol/openid-connect/token```


2. Replace `<TOKEN>` with the access token (access_token attribute in the response) received in the previous step and execute the following command

```curl -X POST -k https://localhost:8080/hub -H "Authorization: Bearer <TOKEN>" -H "Content-Type:Application/json" -d "{\"callback\":\"http://elhost:8081/eventlistener\",\"query\":null}"```

or

```curl -X POST -k https://localhost:8080/hub -H "Authorization: Bearer <TOKEN>" -H "Content-Type:Application/json" -d @add_listener.json```

with the content of the file add_listener.json

```
{
    "callback" : "http://elhost:8081/eventlistener",
    "query" : null
}
```