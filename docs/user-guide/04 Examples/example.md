# Example API use

## GET Requests

### Get all resources

```curl http://127.0.0.1:8080/resourceInventoryManagement/v4.0.0/resource```


### Get all services

```curl http://127.0.0.1:8080/serviceInventoryManagement/v4.0.0/service```

## POST Requests

### Add resource

```curl -X POST -H "Content-Type: application/json" -d @request_resource.json http://127.0.0.1:8080/resourceInventoryManagement/v4.0.0/resource```

Content of the file request_resource.json

```
{
    "name": "resource1",
    "description": "Resource's description",
    "category": "link",
    "@type": "LogicalResource",
    "@schemaLocation": "https://raw.githubusercontent.com/GEANT-NETDEV/Inv3-schema/main/TMF639-ResourceInventory-v4-ext_20240121-1.json"
}
```

Attribute "@schemaLocation" must have the correct local path/url of schema file (see "Request Validation" section of this documentation).

### Add service

```curl -X POST -H "Content-Type: application/json" -d @C:\Users\Desktop\Inventory3\request_service.json http://127.0.0.1:8080/serviceInventoryManagement/v4.0.0/service```

Content of the file request_service.json

```
{
    "serviceType": "Link",
    "name": "name2",
    "description": "Service description",
    "@schemaLocation": "https://raw.githubusercontent.com/GEANT-NETDEV/Inv3-schema/main/TMF638-ServiceInventory-v4.json"
}
```
Attribute "@schemaLocation" must have the correct local path/url of schema file (see "Request Validation" section of this documentation).

## DELETE Requests

### Delete resource

```curl -X DELETE http://127.0.0.1:8080/resourceInventoryManagement/v4.0.0/resource/<ID>```


### Delete service

```curl -X DELETE http://127.0.0.1:8080/serviceInventoryManagement/v4.0.0/service/<ID>```

```<ID>``` is identifier of a resource