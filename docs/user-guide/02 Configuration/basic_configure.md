---
title: "Basic Configuration"
hide:
    - toc
---
# Basic Configuration

|                 Property                 |       Example Values       |                                        Description                                        |
|:----------------------------------------:|:--------------------------:|:-----------------------------------------------------------------------------------------:|
|                mongo-host                |   localhost or 127.0.0.1   |              Hostname for Inventory3 database in the form of name or address              |
|                mongo-user                |            user            |                                   Username for MongoDB                                    |
|              mongo-password              |          password          |                                   Password for MongoDB                                    |
|            resource.protocol             |            http            |                   Remote application server protocol for resource part                    |
|             resource.address             |         10.27.1.10         |                    Remote application server address for resource part                    |
|              resource.port               |            8080            |                     Remote application server port for resource part                      |
|             service.protocol             |            http            |                    Remote application server protocol for service part                    |
|             service.address              |         10.27.1.10         |                    Remote application server address for service part                     |
|               service.port               |            8080            |                      Remote application server port for service part                      |
|            server.ssl.enabled            |         true/false         |                       Enable/Disable https protocol for Inventory3                        |
|             keycloak.enabled             |         true/false         |                    Enable/Disable Keycloak application for Inventory3                     |
|         keycloak.auth-server-url         | http://127.0.0.1:8090/auth |                                  Keycloak server address                                  |
|           resourceService.type           |       base/extended        | Select 'base' version for resource part or 'extended'<br/> Extended version used for PSNC |
|  resourceService.checkExistingResource   |         true/false         |         Enable/Disable check existing resource functionality for extended version         |
| notification.sendNotificationToListeners |         true/false         |                     Enable/Disable sending notifications to listeners                     |
|             openapi.dev-url              |   http://localhost:8080    |                      Base URL development environment for Swagger UI                      |
|             openapi.prod-url             |  https://inventory3:8080   |                      Base URL production environment for Swagger UI                       |

## SSL Configuration

|                 Property                 |  Example Values  |                            Description                            |
|:----------------------------------------:|:----------------:|:-----------------------------------------------------------------:|
|            server.ssl.enabled            |    true/false    |           Enable/Disable https protocol for Inventory3            |
|        server.ssl.key-store-type         |      PKCS12      |                           Keystore type                           |
|           server.ssl.key-store           | src/main.key.p12 |                     The path of keystore file                     |
|      server.ssl.key-store-password       |     test123      |                       Password for keystore                       |
|           server.ssl.key-alias           |   exampleAlias   | The alias (or name) under which the key is stored in the keystore |
