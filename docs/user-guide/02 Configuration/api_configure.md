---
title: "Authentication Configuration"
hide:
    - toc
---
# Authentication Configuration

## API Authentication - Keycloak

|                       Property                        |                                     Example Values                                     |                                              Description                                               |
|:-----------------------------------------------------:|:--------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------:|
|                   keycloak.enabled                    |                                       true/false                                       |                           Enable/Disable Keycloak application for Inventory3                           |
| spring.security.oauth2.resourceserver.jwt.issuer-uri  |                    http://keycloakhost:8090/realms/Inventory3Realm                     |                                       The URL to Keycloak realm                                        |
| spring.security.oauth2.resourceserver.jwt.jwk-set-uri | ${spring.security.oauth2.resourceserver.jwt.issuer-uri}/protocol/openid-connect/certs  |                            JSON Web Key URI to use to verify the JWT token                             |
|          token.converter.principal-attribute          |                                   preferred_username                                   | Parameter that allows to extract the Keycloak user name from a token available on the Spring Boot side |
|              token.converter.resource-id              |                                          inv3                                          |                        The name of the client that Spring Boot application uses                        |
