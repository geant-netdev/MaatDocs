---
title: "SSL Configuration"
hide:
    - toc
---

# SSL Configuration

|                 Property                 |  Example Values  |                            Description                            |
|:----------------------------------------:|:----------------:|:-----------------------------------------------------------------:|
|            server.ssl.enabled            |    true/false    |           Enable/Disable https protocol for Inventory3            |
|        server.ssl.key-store-type         |      PKCS12      |                           Keystore type                           |
|           server.ssl.key-store           | src/main.key.p12 |                     The path of keystore file                     |
|      server.ssl.key-store-password       |     test123      |                       Password for keystore                       |
|           server.ssl.key-alias           |   exampleAlias   | The alias (or name) under which the key is stored in the keystore |
