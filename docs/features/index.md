---
title: "Maat Features"
tags: 
    - Open API
    - Extensible data model
    - Data validation
    - Modular
    - Notification hub
    - Dynamic GUI
hide:
    - toc
    - navigation
---


# Maat Features

Available as a free open-source solution under the [Apache v2.0 license](https://www.apache.org/licenses/LICENSE-2.0.txt), Maat provide you with:

<div class="grid cards" markdown>

-   :material-api:{ .lg .middle } __Open, standards-based API__

    ---

    - Full CRUD support offers automation and orchestration implementation out of the box.
    - [TMF638 Service management API](https://www.tmforum.org/resources/specifications/tmf638-service-inventory-api-user-guide-v5-0-0/)
    - [TMF639 Resource management API](https://www.tmforum.org/resources/standard/tmf639-resource-inventory-api-user-guide-v4-0/)


-   :simple-codeblocks:{ .lg .middle } __Scalable extensible architecture__

    ---

    - Single, consolidated database 
    - Integrated validation and logging
    - Compose complex objects using two-way relationships


-   :simple-coderwall:{ .lg .middle } __Extensible data model__

    ---

    - Industry compatible service and resource base model 
    - Endlessly customisable
    - Model your own resources using templates


-   :fontawesome-solid-puzzle-piece:{ .lg .middle } __Modular structure__

    ---

    - Schema based non-SQL database
    - Separate service and resource APIs
    - Individual event management

-   :material-form-select:{ .lg .middle } __Dynamic GUI__

    ---

    - Easy to use, intuitive design
    - Adapts to your customised model definitions
    - Supports different data views

-   :material-axis-arrow-info:{ .lg .middle } __Event Notification__

    ---

    - Separate notification engine
    - Listen and react to events 
    - Log all activities

</div>


## Maat Components

<div class="grid cards" markdown>

-   :material-api:{ .lg .middle } __Physical Resources__

    ---

    Describe infrastructure elements and details of physical location and utilization (card distribution, allocation of physical ports, …)


-   :simple-codeblocks:{ .lg .middle } __Logical Resources__

    ---

    Describe virtual resources, connections and relationships between resources


-   :simple-coderwall:{ .lg .middle } __Services__

    ---

    Collect detailed information about services and map them to logical and/or physical resources.


-   :fontawesome-solid-puzzle-piece:{ .lg .middle } __Notification Management__

    ---

    Keep history of all changes, subscribe to events of interest.

</div>

[//]: # (## Maat Use cases)

[//]: # (=== "Example Data Models")

[//]: # (    - WRF)
[//]: # (    - L2VPN)
[//]: # (    - ...)

[//]: # (=== "Success Stories")

[//]: # (    - PIONIER )


## Give it a Try

<div class="grid cards" markdown>

-   :material-test-tube:{ .lg .middle } __Run a demo in NMaaS__

    ---

    Maat is available as a container application in NMaaS. Test it's features in a ready to use environment.

     [:octicons-arrow-right-24: Go to NMaaS](https://nmaas.eu/welcome/login)

</div>
<br><br><br>
![decorative hieroglyphs](../assets/images/Maat%20graphics/horizontal%20hieroglyphs/grey2.png)
